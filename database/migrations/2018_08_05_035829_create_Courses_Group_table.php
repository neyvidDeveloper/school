<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesGroupTable extends Migration {

	public function up()
	{
		Schema::create('Courses_Group', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('Courses_Group');
	}
}