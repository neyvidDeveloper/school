<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration {

	public function up()
	{
		Schema::create('Courses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('course_group_id');
			$table->integer('mentor_id');
			$table->string('title');
			$table->string('price');
			$table->string('discount');
			$table->string('day_time');
			$table->dateTime('start_time');
			$table->dateTime('end_time');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('Courses');
	}
}