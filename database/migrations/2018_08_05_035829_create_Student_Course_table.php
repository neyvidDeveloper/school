<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentCourseTable extends Migration
{

    public function up()
    {
        Schema::create('Student_Course', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('course_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('Student_Course');
    }
}