<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{

    public function up()
    {
        Schema::create('Order_Items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('course_id');
            $table->integer('amount');
            $table->integer('discount');
            $table->integer('payable_amount');
            $table->integer('count');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('Order_Items');
    }
}
