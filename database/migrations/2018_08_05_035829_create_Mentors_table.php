<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMentorsTable extends Migration
{

    public function up()
    {
        Schema::create('Mentors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mentor_code');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('last_name');
            $table->string('code_meli');
            $table->string('mobile');
            $table->string('phone');
            $table->string('address');
            $table->integer('score');
            $table->string('picture');
            $table->text('cv');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('Mentors');
    }
}