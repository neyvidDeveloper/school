<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('Orders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('student_id');
			$table->integer('total_amount');
			$table->integer('discount');
			$table->integer('payable_amount');
			$table->integer('payment_method');
			$table->integer('order_status');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('Orders');
	}
}