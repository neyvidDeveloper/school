<?php

namespace App\Models\Course;

use App\Models\CourseGroup\CourseGroup;
use App\Models\Mentor\Mentor;
use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Model;

class Course extends Model 
{

    protected $table = 'Courses';
    public $timestamps = true;

    public function Mentor()
    {
        return $this->belongsTo(Mentor::class);
    }

    public function Course_Group()
    {
        return $this->belongsTo(CourseGroup::class,'Course_group_id');
    }

    public function Students()
    {
        return $this->belongsToMany(Student::class,'Student_Course','course_id','student_id');
    }

}