<?php

namespace App\Models\Mentor;

use App\Models\Course\Course;
use Illuminate\Database\Eloquent\Model;

class Mentor extends Model 
{

    protected $table = 'Mentors';
    public $timestamps = true;

    public function Courses()
    {
        return $this->hasMany(Course::class);
    }

}