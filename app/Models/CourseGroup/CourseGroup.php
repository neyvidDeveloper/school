<?php

namespace App\Models\CourseGroup;

use App\Models\Course\Course;
use Illuminate\Database\Eloquent\Model;

class CourseGroup extends Model 
{

    protected $table = 'Courses_Group';
    public $timestamps = true;

    public function Courses()
    {
        return $this->hasMany(Course::class,'Course_group_id');
    }

}