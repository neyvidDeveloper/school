<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table='Order_Items';

    public function Order()
    {
        $this->belongsTo(Order::class);
    }
}
