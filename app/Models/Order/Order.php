<?php

namespace App\Models\Order;

use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{

    protected $table = 'Orders';
    public $timestamps = true;

    public function Student()
    {
        return $this->belongsTo(Student::class);
    }

    public function Order_Items()
    {
        $this->hasMany(OrderItem::class);
    }

}