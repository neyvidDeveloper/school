<?php

namespace App/Models/StudentCourse;

use Illuminate\Database\Eloquent\Model;

class StudentCourse extends Model 
{

    protected $table = 'Student_Course';
    public $timestamps = true;

}